# Optodesigner PWD

Finding the current working directory in optodesigner:

- To see the problem with this example please open all three files in optodesigner.
- Set the main.spt as the main (ctrl-shift m)
- `pathA` will work just fine
- but `pathB` depends on the teb that is opened in optodesigner.